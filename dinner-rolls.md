# Dinner Rolls #
This recipe is from Helen Rennie's YouTube channel (https://www.youtube.com/watch?v=enYzkr98_8M&feature=emb_logo). It can be used for donuts and other delicious sweet bread recipes.

## Tangzhong Roux ##
* 43g water (any temperature)
* 43g whole milk (any temperature)
* 14g King Arthur Unbleached All-Purpose Flour

## The Dough ##
* 310g King Arthur Unbleached All-Purpose Flour
* 50g granulated sugar
* 2 tsp Diamond Crystal Kosher Salt or 1 tsp Table Salt (5.4 g) 
* 1 Tbsp (7.1 g)  instant yeast
* 113g whole milk, cold
* 1 large egg, cold (50 g without shell)
* 57g unsalted butter, barely melted (lukewarm)

## Egg Wash ##
* 1 egg yolk
* 1 tbsp milk

## Instructions ##
1. Heat butter in microwave until just barely melted. Some chunks ok.
1. Measure out milk into a small measuring cup.
1. Combine all dry ingredients into mixing bowl and whisk to combine.
1. Whisk all roux ingredients (cold) to combine.
1. Heat roux on med-low, and constantly whisk until it's like creamy mashed potatoes.
1. Combine cold milk with roux and whisk to combine.
1. Add egg and whisk to combine.
1. Add roux mixture and butter to the dry ingredients.
1. Mix on low speed until combined.
1. Mix 4 minutes on setting 4.
1. Scrape bowl, flip dough.
1. Mix 4 minutes on setting 4.
1. Scrape bowl, flip dough.
1. Mix 4 minutes on setting 4.
1. Scrape bowl, flip dough.
1. Window pane test, if not ready, repeat last two steps
1. Get dough into round ball with light needing. Fold towards yourself, press, rotate.
1. Let dough rise in a lightly oiled container, let it double in size
1. Put out onto lightly floured surface, flatten dough.
1. Cut dough into 11 evenly sized BAAAALLLS.
1. Preshape dough
1. Roll each chunk into smooth ball, sealed on bottom.
1. Place into greased backing sheet.
1. Cover with plastic, let rise until doubled in size.
1. Set oven to 350F
1. Brush each roll with egg wash
1. Bake 25 mins or until center rolls are 195F
1. Once baked, brush rolls with melted butter.
