# Sourdough Bread #

This recipe is from [Foodgeek](https://www.youtube.com/watch?v=Znv99QbfWGs) on YouTube. I've used it several times, and this is merely a summary of the steps so it's easy to read while making it. This is not an exhaustive recipe, see his [video](https://www.youtube.com/watch?v=Znv99QbfWGs) and [blog](https://foodgeek.dk/en/sourdough-bread-recipe-for-beginners/) for more information.

## Levain ##

Makes 250g of levain, which is more than you need.

* 50g starter
* 50g AP flour
* 50g Spelt flour
* 100g water

## Autolyse Mixture ##

* 686g bread flour
* 152g wheat flour
* 509g water

## Bread ##

* Autolyse Mixture
* 185g levain
* 19g salt
* 50g water

## Instructions ##

1. Make your levain
1. When the levain has grown about 175%, make your autolyse mixture
1. When the levain has doubled, mix all your bread ingredients
1. Let rest 30 mins
1. Stretch and fold #1
1. Rest 30 mins
1. Stretch and fold #2
1. Rest 30 mins
1. Stretch and fold #3
1. Window Pane Test, if not good repeat
1. Rest for 2.5 hours (bulk ferment)
1. Split dough into two loaves
1. Stretch and fold then flip dough over
1. Scrape and push to get taught ball
1. Repeat with other dough
1. Reset under damp towel 20 mins
1. Final Shaping, repeat pre-shaping
1. When ball is taught, flip into banneton taught side down.
1. Sprinkle top with rice flour
1. Place banneton into plastic bag and rest in fridge overnight