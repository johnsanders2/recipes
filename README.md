# Recipes #
These are recipes I like to keep handy and reformat so they work for my cooking styles.

* [Sourdough Bread](sourdough-bread.md)
* [Dinner Rolls](dinner-rolls.md)

## Links ##
* [OVERNIGHT SOURDOUGH BREAKFAST STRATA](https://www.culturesforhealth.com/learn/recipe/sourdough-recipes/overnight-sourdough-breakfast-strata/)
* [10 USES FOR STALE SOURDOUGH BREAD](https://www.culturesforhealth.com/learn/sourdough/ten-uses-stale-sourdough-bread/)
* [Sprouted Grain Sourdough](https://www.baked-theblog.com/sprouted-grain-sourdough-bread/)
* [New York Style Sourdough Bagels](https://www.baked-theblog.com/new-york-style-sourdough-bagels-with-roasted-garlic-labneh/)
* [Edible Cookie Dough](https://preppykitchen.com/edible-cookie-dough/)